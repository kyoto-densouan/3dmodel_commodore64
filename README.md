# README #

1/3スケールのコモドール コモドール64風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- コモドール

## 発売時期
- コモドール64 1982年1月
- VIC-20 1980年
- VIC-1001 1981年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/%E3%82%B3%E3%83%A2%E3%83%89%E3%83%BC%E3%83%AB64)
- [PC-HISTORY](http://www.pc-history.org/comm.htm)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_commodore64/raw/dd6d3ef5f9937d4b0290f716611a33d922cd5ce0/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_commodore64/raw/dd6d3ef5f9937d4b0290f716611a33d922cd5ce0/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_commodore64/raw/dd6d3ef5f9937d4b0290f716611a33d922cd5ce0/ExampleImage.png)
